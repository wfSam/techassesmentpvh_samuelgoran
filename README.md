# Tech Assesment - PVH

The assesment consists in writing an automated test suite for the following public website: https://nl.tommy.com/

The tests should cover the following functionality: **Create a new account** 

1. Verify that user could see specific errors when not enough information is provided during registration process
2. Verify that user can create an account with valid data
3. Verify that registered user can add a new address

We will be using [Faker](https://www.npmjs.com/package/Faker), to generate random test data, such as *email address, person address details, person details*

---

## Setup pre-requisite

1. Download & Install NodeJS from https://nodejs.org/en/download/

2. Download you preffered text editor. In my case I used Visual Studio Code (code.visualstudio.com)

3. Clone the repository

4. Open the project in Visual Studio Code

5. Install dependencies
	- From the terminal, execute:
	```
	npm install
	```
---

## How to execute the tests
1. To debug the test:
	```
	npm run debug
	# Select "tech_assesment_pvh.js" to trigger the test suite
	```

5. To run the test:
	```
	npm run test
	```
---

## Verify results
You can check any errors in the **screenshots** folder under cypress.

Or a full video recording of the execution in the **cypress/videos** folder.
