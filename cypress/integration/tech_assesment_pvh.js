const faker = require('faker')

const goodEmail = `${faker.internet.userName()}@mailinator.com`
const password = faker.internet.password()

describe('Create new account', () => {
    before(() => {
        cy.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })

        cy.setCookie('PVH_COOKIES_GDPR_ANALYTICS', 'Accept', { domain: 'nl.tommy.com', path: '/' })
        cy.setCookie('PVH_COOKIES_GDPR', 'Accept', { domain: 'nl.tommy.com', path: '/' })
        cy.setCookie('PVH_COOKIES_GDPR_SOCIALMEDIA', 'Accept', { domain: 'nl.tommy.com', path: '/' })

        cy.visit('https://nl.tommy.com/', { timeout: 120000 }) // 2 mins in miliseconds

        cy.get('.nav__toggle-label')
            .click();

        cy.get('.sl-sign-in-or-register')
            .click()

        cy.get('button[data-testid=register]')
            .click();
    });

    it('1.Verify that the user could see specific errors when not enough information is provided during registration process', () => {
        //missing email
        cy.get('#create-account-password')
            .type('ceva')
        cy.get('#create-account-email-helper-text')
            .should('be.visible')
        //incorect email
        cy.get('#create-account-email')
            .type('incompleteemail')
        cy.get('#create-account-password')
            .type('password')
        cy.get('#create-account-email-helper-text')
            .should('be.visible')
            // .should('contain', 'missing password in NL')
        //missing password
        cy.get('#create-account-email')
            .clear()
            .type('samuel.goran@gmail.com')
        cy.get('#create-account-password')
            .clear()
        cy.get('.agree-terms > [data-testid=checkbox-label]')
            .click()
        cy.get('#create-account-password-helper-text')
            .should('be.visible')
    })

    it('2.Verify that the use can create an account with valid data', () => {

        //fill in email
        cy.get('#create-account-email')
            .clear()
            .type(goodEmail)
        //fill in password
        cy.get('#create-account-password')
            .clear()
            .type(password)
        cy.log(`I have created an account with user: ${goodEmail} and password: ${password}`)

        cy.get('button[data-testid=Button-primary-new]')
            .click({ timeout: 120000 })

    })

    it('3.Verify that registered user can add a new address', () => {
        // PORTAL contains JS errors and we need to ignore them so the test continues. 
        // In a normal scenario, the test fails as of the identified error on the AUT.
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        cy.intercept('https://nl.tommy.com/wcs/resources/store/**').as('createAddress');

        cy.setCookie('PVH_COOKIES_GDPR_ANALYTICS', 'Accept', { domain: 'nl.tommy.com', path: '/' })
        cy.setCookie('PVH_COOKIES_GDPR', 'Accept', { domain: 'nl.tommy.com', path: '/' })
        cy.setCookie('PVH_COOKIES_GDPR_SOCIALMEDIA', 'Accept', { domain: 'nl.tommy.com', path: '/' })

        cy.get('.my-account__sidebar a[href*="/myaccount/addressbook"]', { timeout: 180000 }).click()
        cy.get('.newsletter__close.button--close', { timeout: 180000 }).click({ force: true })
        cy.get('.Button.Button__secondary[data-testid*="address-add-button"]').click()

        cy.get('#firstName').clear().type(faker.name.firstName())
        cy.get('#lastName').clear().type(faker.name.lastName())
        const addressName = faker.address.streetName();
        cy.get('#address1').clear().type(addressName)
        cy.get('#address2').clear().type(faker.address.secondaryAddress())
        cy.get('#city').clear().type('Amsterdam')
        cy.get('#zipCode').clear().type('1111 AB')
        cy.get('#phone1').clear().type('07847348937')
        cy.get('#country').select('NL')
        cy.get('button[data-testid*="address-save-button"]').click()

        cy.wait('@createAddress').its('response.statusCode').should('eq', 200);

        cy.get('.address-book__form > select > option', { timeout: 120000 }).should('contain', addressName)
    })

})
